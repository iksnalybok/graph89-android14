LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := array-test

LOCAL_SHARED_LIBRARIES := libglib-2.0 libgthread-2.0

LOCAL_SRC_FILES :=  \
    array-test.c

base := $(LOCAL_PATH)/..

LOCAL_C_INCLUDES :=  \
    $(base)  \
    $(base)/android  \
    $(base)/glib

include $(BUILD_EXECUTABLE)
