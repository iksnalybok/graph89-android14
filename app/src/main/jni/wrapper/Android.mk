LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := wrapper

LOCAL_SHARED_LIBRARIES := glib-2.0 ticables2-1.3.3 ticonv-1.1.3 tifiles2-1.1.5 ticalcs2-1.1.7 tiemu-3.03 tilem-2.0

LOCAL_SRC_FILES :=  \
    wrappercommon.c  \
    tilemwrapper.c  \
    tiemuwrapper.c  \
    wrappercommonjni.c  \
    tiemuwrapperjni.c  \
    tilemwrapperjni.c  \
    wabbitvar.c  \
    wabbitlink.c  \
    bootimage.c

LOCAL_CFLAGS :=  \
    -I$(LOCAL_PATH)  \
    \
    -O3

LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog

include $(BUILD_SHARED_LIBRARY)
