LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := ticonv-1.1.3

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/src

LOCAL_SHARED_LIBRARIES := glib-2.0

LOCAL_SRC_FILES :=  \
    src/ticonv.c  \
    src/charset.c  \
    src/filename.c  \
    src/tokens.c

LOCAL_CFLAGS :=  \
    -I$(LOCAL_PATH)/src  \
    \
    -DHAVE_CONFIG_H  \
    -DTICONV_EXPORTS  \
    -O3

include $(BUILD_SHARED_LIBRARY)
